//
//  EntryViewController.m
//  JournalAppObjC
//
//  Created by Garrick McMickell on 10/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import "EntryViewController.h"

@interface EntryViewController ()

@end

@implementation EntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.positionInArray) {
        [self loadData];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)loadData {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray* journalEntries = [[defaults objectForKey:@"journalEntries"] mutableCopy];
    NSDictionary* d = journalEntries[self.positionInArray.row];
    
    self.textField.text = d[@"title"];
    self.textView.text = d[@"content"];
}

- (IBAction)saveBtnTouched:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary* d = @{
                        @"title":self.textField.text,
                        @"content":self.textView.text
                        };
    
    NSMutableArray* journalEntries = [[defaults objectForKey:@"journalEntries"] mutableCopy];
    
    if(!journalEntries) {
        journalEntries = [NSMutableArray new];
    }
    
    if(self.positionInArray) {
        [journalEntries replaceObjectAtIndex:self.positionInArray.row withObject:d];
    }
    else {
        [journalEntries addObject:d];
    }
    
    [defaults setObject:journalEntries forKey:@"journalEntries"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)keyboardWasShown: (NSNotification*) notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _textView.contentInset = contentInsets;
    _textView.scrollIndicatorInsets = contentInsets;
    
    CGRect rect = self.view.frame;
    rect.size.height = kbSize.height;
    
    if(!CGRectContainsPoint(rect, _textView.frame.origin)){
        [self.textView scrollRectToVisible:_textView.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden: (NSNotification*) notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _textView.contentInset = contentInsets;
    _textView.scrollIndicatorInsets = contentInsets;
}

@end
