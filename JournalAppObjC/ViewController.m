//
//  ViewController.m
//  JournalAppObjC
//
//  Created by Garrick McMickell on 10/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void) loadData {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    journalEntries = [defaults objectForKey:@"journalEntries"];
    [self.myTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addBtnTouched:(id)sender {
    [self performSegueWithIdentifier:@"moveToDetail" sender:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return journalEntries.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if(!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* d = journalEntries[indexPath.row];
    
    cell.textLabel.text = d[@"title"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"moveToDetail" sender:indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"moveToDetail"]) {
        EntryViewController* evc = (EntryViewController*)segue.destinationViewController;
        if(sender) {
            NSIndexPath* ip = (NSIndexPath*)sender;
            evc.positionInArray = ip;
        }
    }
}

@end
