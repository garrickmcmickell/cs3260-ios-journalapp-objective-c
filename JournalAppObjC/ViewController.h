//
//  ViewController.h
//  JournalAppObjC
//
//  Created by Garrick McMickell on 10/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntryViewController.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    NSArray* journalEntries;
}

@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@end

