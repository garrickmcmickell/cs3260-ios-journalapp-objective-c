//
//  EntryViewController.h
//  JournalAppObjC
//
//  Created by Garrick McMickell on 10/5/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntryViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@property (strong, nonatomic) NSIndexPath* positionInArray;

@end
